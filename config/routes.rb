Rails.application.routes.draw do
  root 'static_pages#home'
  resources :participants
  resources :coordinators
  resources :registries
  resources :enrollment_to_registries
end
