# Reistry Application

This is a application to manage users in a registry.


## Provide sql for aggregate count of participants by year of birth.

```
SELECT DATEPART(year,DATE_FORMAT(dob, '%Y-%m-%d ')), COUNT(DISTINCT participant_id) FROM participants GROUP BY DATEPART(year,DATE_FORMAT(dob, '%Y-%m-%d '));

```

## Provide sql for aggregate count of participants by coordinator and gender.


```
SELECT coordinator, gender, COUNT(DISTINCT participant_id) FROM participants GROUP BY coordinator, gender;

```

## Export participants for each coordinator in a csv format.

Could not complete. I would use jquery datatables library to view the joined tables. The library provides functionality for user to download. Similar to https://datatables.net/extensions/buttons/examples/initialisation/export.html

## To Do

* Add validations while adding participants

* Recursive delete when registry is deleted

* Write unit tests

* Cleaner UI

* Export functionality



## Getting started

To get started with the app, clone the repo and then install the needed gems:

```
$ bundle install --without production
```

Next, migrate the database:

```
$ rails db:migrate
```

Finally, run the test suite to verify that everything is working correctly:

```
$ rails test
```

If the test suite passes, you'll be ready to run the app in a local server:
```
$ rails server
```

