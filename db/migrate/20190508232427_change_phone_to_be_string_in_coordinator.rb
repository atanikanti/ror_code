class ChangePhoneToBeStringInCoordinator < ActiveRecord::Migration[5.1]
  def change
  	change_column :coordinators, :phonenumber, :string
  end
end
