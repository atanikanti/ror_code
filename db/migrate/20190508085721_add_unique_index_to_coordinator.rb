class AddUniqueIndexToCoordinator < ActiveRecord::Migration[5.1]
  def change
  	  add_index :coordinators, :emailaddress, unique: true
  end
end
