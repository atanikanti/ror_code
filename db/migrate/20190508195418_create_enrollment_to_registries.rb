class CreateEnrollmentToRegistries < ActiveRecord::Migration[5.1]
  def change
    create_table :enrollment_to_registries do |t|
      t.references :registry, foreign_key: true
      t.references :coordinator, foreign_key: true
      t.references :participant, foreign_key: true

      t.timestamps
    end
  end
end
