class AddUniqueIndexToRegistry < ActiveRecord::Migration[5.1]
  def change
  	add_index :registries, [:name, :location], unique: true
  end
end
