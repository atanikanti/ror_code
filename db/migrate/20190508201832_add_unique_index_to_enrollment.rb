class AddUniqueIndexToEnrollment < ActiveRecord::Migration[5.1]
  def change
  	add_index :enrollment_to_registries, [:registry_id, :participant_id], unique: true,
  	:name => 'Enrollment_Registry_Index'
  end
end
