class ChangeNameToBeStringInCoordinator < ActiveRecord::Migration[5.1]
  def change
  	change_column :coordinators, :name, :string
  end
end
