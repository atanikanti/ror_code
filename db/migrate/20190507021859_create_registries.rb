class CreateRegistries < ActiveRecord::Migration[5.1]
  def change
    create_table :registries do |t|
      t.string :name, :null => false 
      t.string :location, :null => false
      t.boolean :isactive, :null => false

      t.timestamps
    end
  end
end
