class CreateParticipants < ActiveRecord::Migration[5.1]
  def change
    create_table :participants do |t|
      t.string :name
      t.string :gender
      t.date :dob
      t.date :dateofenrollment
      t.string :emailaddress
      t.integer :phonenumber
      t.text :remarks
      t.string :coordinator

      t.timestamps
    end
  end
end
