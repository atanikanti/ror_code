class ChangePhoneToBeStringInParticipant < ActiveRecord::Migration[5.1]
  def change
  	change_column :participants, :phonenumber, :string
  end
end
