json.extract! registry, :id, :name, :location, :isactive, :created_at, :updated_at
json.url registry_url(registry, format: :json)
