json.extract! coordinator, :id, :name, :emailaddress, :phonenumber, :created_at, :updated_at
json.url coordinator_url(coordinator, format: :json)
