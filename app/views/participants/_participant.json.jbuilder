json.extract! participant, :id, :name, :gender, :dob, :dateofenrollment, :emailaddress, :phonenumber, :remarks, :coordinator, :created_at, :updated_at
json.url participant_url(participant, format: :json)
