class Registry < ApplicationRecord
	validates_presence_of :name
	validates_presence_of :location
	validates_presence_of :isactive
	has_many :coordinators
	has_many :participants, :through => :coordinators
	validates :name, uniqueness: { scope: :location }
end
