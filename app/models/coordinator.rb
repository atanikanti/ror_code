class Coordinator < ApplicationRecord
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	validates :emailaddress, presence: true, length: { maximum: 255 }, format: { with: VALID_EMAIL_REGEX }, uniqueness: true
	validates :phonenumber, format: { with: /\d{3}-\d{3}-\d{4}/, message: "bad format. Try 123-456-7890" }
	has_many :registry
	has_many :participant
end
