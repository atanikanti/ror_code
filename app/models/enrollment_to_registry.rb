class EnrollmentToRegistry < ApplicationRecord
  belongs_to :registry
  belongs_to :coordinator
  belongs_to :participant
  validates :registry_id, uniqueness: { scope: :participant_id }
end
