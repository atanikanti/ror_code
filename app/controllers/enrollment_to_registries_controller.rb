class EnrollmentToRegistriesController < ApplicationController
  before_action :set_enrollment_to_registry, only: [:show, :edit, :update, :destroy]

  # GET /enrollment_to_registries
  # GET /enrollment_to_registries.json
  def index
    @enrollment_to_registries = EnrollmentToRegistry.all
  end

  # GET /enrollment_to_registries/1
  # GET /enrollment_to_registries/1.json
  def show
  end

  # GET /enrollment_to_registries/new
  def new
    @enrollment_to_registry = EnrollmentToRegistry.new
  end

  # GET /enrollment_to_registries/1/edit
  def edit
  end

  # POST /enrollment_to_registries
  # POST /enrollment_to_registries.json
  def create
    @enrollment_to_registry = EnrollmentToRegistry.new(enrollment_to_registry_params)

    respond_to do |format|
      if @enrollment_to_registry.save
        format.html { redirect_to @enrollment_to_registry, notice: 'Enrollment to registry was successfully created.' }
        format.json { render :show, status: :created, location: @enrollment_to_registry }
      else
        format.html { render :new }
        format.json { render json: @enrollment_to_registry.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /enrollment_to_registries/1
  # PATCH/PUT /enrollment_to_registries/1.json
  def update
    respond_to do |format|
      if @enrollment_to_registry.update(enrollment_to_registry_params)
        format.html { redirect_to @enrollment_to_registry, notice: 'Enrollment to registry was successfully updated.' }
        format.json { render :show, status: :ok, location: @enrollment_to_registry }
      else
        format.html { render :edit }
        format.json { render json: @enrollment_to_registry.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /enrollment_to_registries/1
  # DELETE /enrollment_to_registries/1.json
  def destroy
    @enrollment_to_registry.destroy
    respond_to do |format|
      format.html { redirect_to enrollment_to_registries_url, notice: 'Enrollment to registry was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_enrollment_to_registry
      @enrollment_to_registry = EnrollmentToRegistry.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def enrollment_to_registry_params
      params.fetch(:enrollment_to_registry, {})
    end
end
